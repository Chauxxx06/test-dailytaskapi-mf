using System.ComponentModel.DataAnnotations;

namespace dailyTask_api.models
{
    public class User
    {
        [Required]
        public Guid UserId {set;get;}
        [Required]
        [MinLength(5, ErrorMessage = "The name must be at least 5 characters long")]
        public string Name {set;get;}
        [Required]
        [MinLength(5, ErrorMessage = "The nickname must be at least 5 characters long")]
        public string Nickname {set;get;}
        [Required]
        [EmailAddress(ErrorMessage ="Invalid email")]
        //[RegularExpression("[a-z0-9._%+-]@[a-z0-9.-]+\\.[a-z]{2,4}", ErrorMessage = "Please enter correct email")]
        public string Email {set;get;}
        [Required]
        [MinLength(8, ErrorMessage = "The password must be at least 8 characters long")]
        public string Password {set;get;}
        public DateTime DateCreated {set;get;}
        public DateTime DateUpdate {set;get;}
    }




}