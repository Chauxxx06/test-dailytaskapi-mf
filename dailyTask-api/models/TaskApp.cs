using System.ComponentModel.DataAnnotations;

namespace dailyTask_api.models
{
    public class TaskApp
    {
        [Required]
        public Guid TaskId {set;get;}
        [Required]
        public string NameUser {set;get;}
        [Required]
        public string Position {set;get;}
        [Required]
        public string TaskName {set;get;}
        public string Description {set;get;}
        [Required]
        public int TimeTask {set;get;}
        public DateTime DateCreated {set;get;}
    }
}