﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace dailyTask_api.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "taskapp",
                columns: table => new
                {
                    TaskId = table.Column<Guid>(type: "TEXT", nullable: false),
                    NameUser = table.Column<string>(type: "TEXT", nullable: false),
                    Position = table.Column<string>(type: "TEXT", nullable: false),
                    TaskName = table.Column<string>(type: "TEXT", nullable: false),
                    Description = table.Column<string>(type: "TEXT", nullable: true),
                    TimeTask = table.Column<int>(type: "INTEGER", nullable: false),
                    DateCreated = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_taskapp", x => x.TaskId);
                });

            migrationBuilder.InsertData(
                table: "taskapp",
                columns: new[] { "TaskId", "DateCreated", "Description", "NameUser", "Position", "TaskName", "TimeTask" },
                values: new object[] { new Guid("4844bfac-29f8-4ad6-bce0-52004718d102"), new DateTime(2023, 1, 14, 13, 33, 59, 596, DateTimeKind.Utc).AddTicks(5660), "tecnicas de phishing", "Pablo P", "DevOps", "Capacitacion de ciberseguridad", 5 });

            migrationBuilder.InsertData(
                table: "taskapp",
                columns: new[] { "TaskId", "DateCreated", "Description", "NameUser", "Position", "TaskName", "TimeTask" },
                values: new object[] { new Guid("4844bfac-29f8-4ad6-bce0-52004718d1cd"), new DateTime(2023, 1, 14, 13, 33, 59, 596, DateTimeKind.Utc).AddTicks(5653), "proyecto de acutalizacion", "Pedro P", "Developer", "Reunion diaria Scrum", 2 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "taskapp");
        }
    }
}
