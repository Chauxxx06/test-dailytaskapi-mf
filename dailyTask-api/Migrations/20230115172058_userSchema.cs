﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace dailyTask_api.Migrations
{
    public partial class userSchema : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "userTable",
                columns: table => new
                {
                    UserId = table.Column<Guid>(type: "TEXT", nullable: false),
                    Name = table.Column<string>(type: "TEXT", nullable: false),
                    Nickname = table.Column<string>(type: "TEXT", nullable: false),
                    Email = table.Column<string>(type: "TEXT", nullable: false),
                    Password = table.Column<string>(type: "TEXT", nullable: false),
                    DateCreated = table.Column<DateTime>(type: "TEXT", nullable: false),
                    DateUpdate = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_userTable", x => x.UserId);
                });

            migrationBuilder.UpdateData(
                table: "taskapp",
                keyColumn: "TaskId",
                keyValue: new Guid("4844bfac-29f8-4ad6-bce0-52004718d102"),
                column: "DateCreated",
                value: new DateTime(2023, 1, 15, 17, 20, 58, 251, DateTimeKind.Utc).AddTicks(309));

            migrationBuilder.UpdateData(
                table: "taskapp",
                keyColumn: "TaskId",
                keyValue: new Guid("4844bfac-29f8-4ad6-bce0-52004718d1cd"),
                column: "DateCreated",
                value: new DateTime(2023, 1, 15, 17, 20, 58, 251, DateTimeKind.Utc).AddTicks(303));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "userTable");

            migrationBuilder.UpdateData(
                table: "taskapp",
                keyColumn: "TaskId",
                keyValue: new Guid("4844bfac-29f8-4ad6-bce0-52004718d102"),
                column: "DateCreated",
                value: new DateTime(2023, 1, 14, 13, 33, 59, 596, DateTimeKind.Utc).AddTicks(5660));

            migrationBuilder.UpdateData(
                table: "taskapp",
                keyColumn: "TaskId",
                keyValue: new Guid("4844bfac-29f8-4ad6-bce0-52004718d1cd"),
                column: "DateCreated",
                value: new DateTime(2023, 1, 14, 13, 33, 59, 596, DateTimeKind.Utc).AddTicks(5653));
        }
    }
}
