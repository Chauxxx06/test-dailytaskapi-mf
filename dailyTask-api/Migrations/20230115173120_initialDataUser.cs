﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace dailyTask_api.Migrations
{
    public partial class initialDataUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "taskapp",
                keyColumn: "TaskId",
                keyValue: new Guid("4844bfac-29f8-4ad6-bce0-52004718d102"),
                column: "DateCreated",
                value: new DateTime(2023, 1, 15, 17, 31, 20, 251, DateTimeKind.Utc).AddTicks(1703));

            migrationBuilder.UpdateData(
                table: "taskapp",
                keyColumn: "TaskId",
                keyValue: new Guid("4844bfac-29f8-4ad6-bce0-52004718d1cd"),
                column: "DateCreated",
                value: new DateTime(2023, 1, 15, 17, 31, 20, 251, DateTimeKind.Utc).AddTicks(1696));

            migrationBuilder.InsertData(
                table: "userTable",
                columns: new[] { "UserId", "DateCreated", "DateUpdate", "Email", "Name", "Nickname", "Password" },
                values: new object[] { new Guid("121d2a52-7d0e-41e4-ab4b-dbd6f392192c"), new DateTime(2023, 1, 15, 17, 31, 20, 251, DateTimeKind.Utc).AddTicks(2783), new DateTime(2023, 1, 15, 17, 31, 20, 251, DateTimeKind.Utc).AddTicks(2783), "elPerro@mail.com", "Pablo P", "El Perro", "1234567890" });

            migrationBuilder.InsertData(
                table: "userTable",
                columns: new[] { "UserId", "DateCreated", "DateUpdate", "Email", "Name", "Nickname", "Password" },
                values: new object[] { new Guid("bfa45824-c874-4485-9b74-0fe12ef14747"), new DateTime(2023, 1, 15, 17, 31, 20, 251, DateTimeKind.Utc).AddTicks(2769), new DateTime(2023, 1, 15, 17, 31, 20, 251, DateTimeKind.Utc).AddTicks(2769), "elGato@mail.com", "Pedro P", "El Gato", "1234567890" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "userTable",
                keyColumn: "UserId",
                keyValue: new Guid("121d2a52-7d0e-41e4-ab4b-dbd6f392192c"));

            migrationBuilder.DeleteData(
                table: "userTable",
                keyColumn: "UserId",
                keyValue: new Guid("bfa45824-c874-4485-9b74-0fe12ef14747"));

            migrationBuilder.UpdateData(
                table: "taskapp",
                keyColumn: "TaskId",
                keyValue: new Guid("4844bfac-29f8-4ad6-bce0-52004718d102"),
                column: "DateCreated",
                value: new DateTime(2023, 1, 15, 17, 20, 58, 251, DateTimeKind.Utc).AddTicks(309));

            migrationBuilder.UpdateData(
                table: "taskapp",
                keyColumn: "TaskId",
                keyValue: new Guid("4844bfac-29f8-4ad6-bce0-52004718d1cd"),
                column: "DateCreated",
                value: new DateTime(2023, 1, 15, 17, 20, 58, 251, DateTimeKind.Utc).AddTicks(303));
        }
    }
}
