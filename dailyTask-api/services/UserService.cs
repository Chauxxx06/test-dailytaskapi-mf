using dailyTask_api.models;

namespace dailyTask_api.services
{
    public class UserService:IUserService
    {
        TaskAppContext context;
        public UserService(TaskAppContext dbContext)
        {
            context = dbContext;
        }

        public IEnumerable<User> Get()
        {
            return context.user;
        }

        public User GetUnique(string email)
        {
            var actualUser = context.user.Where(d => d.Email == email).FirstOrDefault();
            return actualUser;
        }

        public async void Post(User userNew)
        {
            userNew.UserId = Guid.NewGuid();
            userNew.DateCreated = DateTime.UtcNow;
            userNew.DateUpdate = DateTime.UtcNow;
            var passwordTxt = userNew.Password;
            userNew.Password = BCrypt.Net.BCrypt.HashPassword(passwordTxt);
            context.Add(userNew);
            await context.SaveChangesAsync();
        }

        public bool Login(userLoginDTO loginUser)
        {
            User user =  context.user.Where(d => d.Email == loginUser.Email).FirstOrDefault();
            return BCrypt.Net.BCrypt.Verify(loginUser.Password, user.Password);
        }
    }

    public interface IUserService
    {
        IEnumerable<User> Get();
        User GetUnique(string email);
        void Post(User userNew);
        bool Login(userLoginDTO loginUser);
    }
}