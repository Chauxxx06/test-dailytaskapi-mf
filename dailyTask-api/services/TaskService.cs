using dailyTask_api.models;

namespace dailyTask_api.services
{
    public class TaskAppService:ITaskAppService
    {
        TaskAppContext context;
        public TaskAppService(TaskAppContext dbContext)
        {   
            context = dbContext;
            context.Database.EnsureCreated();
        }

        public IEnumerable<TaskApp> Get()
        {
            return context.taskApp;
        }

        public TaskApp GetUnique(Guid id)
        {
            var actualTaskApp = context.taskApp.Find(id);
            return actualTaskApp;
        }

        public async void Post(TaskApp newTaskApp)
        {
            newTaskApp.DateCreated = DateTime.UtcNow;
            context.Add(newTaskApp);
            await context.SaveChangesAsync();
        }

        public async void Update(Guid id, TaskApp taskApp)
        {
            var actualTaskApp = context.taskApp.Find(id);
            if(actualTaskApp != null)
            {
                actualTaskApp.NameUser = taskApp.NameUser;
                actualTaskApp.Position = taskApp.Position;
                actualTaskApp.TaskName = taskApp.TaskName;
                actualTaskApp.Description = taskApp.Description;
                actualTaskApp.DateCreated = DateTime.UtcNow;
                await context.SaveChangesAsync();
            }
        }

        public async void Delete(Guid id)
        {
            var actualTaskApp = context.taskApp.Find(id);
            if(actualTaskApp != null)
            {
                context.Remove(actualTaskApp);
                await context.SaveChangesAsync();
            }
        }
    }
    public interface ITaskAppService
    {
        IEnumerable<TaskApp> Get();
        TaskApp GetUnique(Guid id);
        void Post(TaskApp taskApp);
        void Update(Guid id, TaskApp taskApp);
        void Delete(Guid id);
    }
}