using AutoMapper;
using dailyTask_api.models;

namespace dailyTask_api.profile
{
    public class UserProfile: Profile{
        public UserProfile() {
            CreateMap<userCreationDTO,User>();
            CreateMap<User,userDTO>().ReverseMap();
        }
    }
}