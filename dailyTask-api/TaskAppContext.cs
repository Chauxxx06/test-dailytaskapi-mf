using dailyTask_api.models;
using Microsoft.EntityFrameworkCore;

namespace dailyTask_api
{
    public class TaskAppContext: DbContext
    {
        public DbSet<TaskApp> taskApp {set;get;}
        public DbSet<User> user {set;get;}
        public TaskAppContext(DbContextOptions<TaskAppContext> options): base(options) {}
        
        protected  override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TaskApp>(taskapp => {
                List<TaskApp> taskAppInit = new List<TaskApp>();
                taskAppInit.Add(new TaskApp() {TaskId = Guid.Parse("4844bfac-29f8-4ad6-bce0-52004718d1cd"), NameUser = "Pedro P", Position = "Developer", TaskName = "Reunion diaria Scrum", Description = "proyecto de acutalizacion", TimeTask = 2 ,DateCreated = DateTime.UtcNow});
                taskAppInit.Add(new TaskApp() {TaskId = Guid.Parse("4844bfac-29f8-4ad6-bce0-52004718d102"), NameUser = "Pablo P", Position = "DevOps", TaskName = "Capacitacion de ciberseguridad", Description = "tecnicas de phishing", TimeTask = 5, DateCreated = DateTime.UtcNow});
                taskapp.ToTable("taskapp");
                taskapp.HasKey(p => p.TaskId);
                taskapp.Property(p => p.NameUser).IsRequired();
                taskapp.Property(p => p.Position).IsRequired();
                taskapp.Property(p => p.TaskName).IsRequired();
                taskapp.Property(p => p.Description);
                taskapp.Property(p => p.TimeTask);
                taskapp.Property(p => p.DateCreated);
                taskapp.HasData(taskAppInit);
            });

            modelBuilder.Entity<User>(u => {
                List<User> userInit = new List<User>();
                userInit.Add(new User() {UserId = Guid.NewGuid(), Name = "Pedro P", Nickname = "El Gato", Email = "elGato@mail.com", Password = "1234567890", DateCreated = DateTime.UtcNow, DateUpdate = DateTime.UtcNow});
                userInit.Add(new User() {UserId = Guid.NewGuid(), Name = "Pablo P", Nickname = "El Perro", Email = "elPerro@mail.com", Password = "1234567890", DateCreated = DateTime.UtcNow, DateUpdate = DateTime.UtcNow});
                u.ToTable("userTable");
                u.HasKey(p => p.UserId);
                u.Property(p => p.Name).IsRequired();
                u.Property(p => p.Nickname).IsRequired();
                u.Property(p => p.Email).IsRequired();
                u.Property(p => p.Password).IsRequired();
                u.Property(p => p.DateCreated).IsRequired();
                u.Property(p => p.DateUpdate).IsRequired();
                u.HasData(userInit);
            });
        }
    }
}