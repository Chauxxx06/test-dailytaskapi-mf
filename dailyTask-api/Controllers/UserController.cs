using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using dailyTask_api.models;
using dailyTask_api.services;
using AutoMapper;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.IdentityModel.Tokens.Jwt;

namespace dailyTask_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UserController : ControllerBase
    {
        IUserService userService;
        private IMapper Mapper {get;}
        public UserController(IUserService service, IMapper mapper)
        {
            userService = service;
            this.Mapper = mapper;
        }

        [HttpGet("{id}")]
        public IActionResult GetUnique([FromRoute] string id)
        {
            var userDB = userService.GetUnique(id);
            var userDTOGet = Mapper.Map<userDTO>(userDB);
            return Ok(userDTOGet);
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok(userService.Get());
        }

        [HttpPost]
        public IActionResult Post([FromBody] userCreationDTO userDTONew)
        {
            if(ModelState.IsValid)
            {
                var newUser = Mapper.Map<User>(userDTONew);
                userService.Post(newUser);
                return Ok("User Created!");
            }
            else{
                return BadRequest();
            } 
        }

        [HttpPost]
        [Route("login")]
        public IActionResult Login([FromBody] userLoginDTO userLogin)
        {
            var verify = userService.Login(userLogin);
            if(!verify)
            {
                return Forbid();
            }
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes("S3cr3t_K3y!S3cr3t_K3y!S3cr3t_K3y!S3cr3t_K3y!S3cr3t_K3y!");
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] { new Claim("id", userLogin.Email) }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var jwt = tokenHandler.CreateToken(tokenDescriptor);
            return Ok(new {AccessToken = tokenHandler.WriteToken(jwt)});
        }
    }
}