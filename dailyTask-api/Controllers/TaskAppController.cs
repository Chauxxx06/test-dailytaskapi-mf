using System.Net;
using System.Net.Http;
using Microsoft.AspNetCore.Mvc;
using dailyTask_api.models;
using dailyTask_api.services;

namespace dailyTask_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TaskAppController : ControllerBase
    {
        ITaskAppService taskAppService;
        public TaskAppController(ITaskAppService service)
        {
            taskAppService = service;
        }

        [HttpGet("{id}")]
        public IActionResult GetUnique([FromRoute] Guid id)
        {
            return Ok(taskAppService.GetUnique(id));
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok(taskAppService.Get());
        }

        [HttpPost]
        public IActionResult Post([FromBody] TaskApp taskAppNew)
        {
            if(ModelState.IsValid)
            {
                taskAppService.Post(taskAppNew);
                return Ok();
            }else{
                return BadRequest();
            }
        }

        [HttpPut("{id}")]
        public IActionResult Put([FromRoute] Guid id, [FromBody] TaskApp taskAppNew)
        {
            if(ModelState.IsValid)
            {
                taskAppService.Update(id, taskAppNew);
                return Ok("Data actualizada");
            }else{
                return BadRequest();
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete([FromRoute] Guid id)
        {
            taskAppService.Delete(id);
            return Ok("dato eliminado");
        }   
    }
}