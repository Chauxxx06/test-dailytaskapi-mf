## API daily task

- Desarrollado con net 6.0.4 y entity framework core InMemory y SQLite
- Desarrollado bajo modelo MVC.
- el programa tiene dependencias de EF InMemory y SQLite, las pruebas se realizaron con InMemory y despues se configurò para que funcionara con SQLite. Las migraciones quedaan dentro del repositorio, asi como el archivo de la base de datos
- para ingresar al swagger se ingresa la direccion https://localhost:7126/swagger
- El NLog esta configurado, sin embargo la api esta desarrollada enteramente en Linux, lo cual me genero problemas al ejecutarlo. Queda comentariado las instrucciones de configuracion.

## API Login

- Se crea APi de Login con una clase que indica toda la informacion que puede tener un usuario en el sistema
- Se crea endpoint de GET para traerse a todos los usuarios con el fin de validar que la informacion esta quedado almacenada; se debe inhabilitar una vez este en produccion
- Se crean DTO's para la creacion de usuarios y consulta del mismo, ademas se crea DTO para el LOGIN
- Se programa el POST para almacenar usuariois nuevos
- Se incluye libreria BCRypt para manejar encriptacion de password en la base de datos
- Se incluye librerias JWT para envio de token de autorizacion
- Se testea el endpoint de login para que valide si el passwqord es correcto y si es asi, genera el JWT para menjarlo en el Frontend
